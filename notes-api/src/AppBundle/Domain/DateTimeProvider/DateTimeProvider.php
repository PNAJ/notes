<?php
namespace  AppBundle\Domain\DateTimeProvider;

class DateTimeProvider
{
    /**
     * @return \DateTime
     */
    public function get()
    {
        return new \DateTime;
    }
}
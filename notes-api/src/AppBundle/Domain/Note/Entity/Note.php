<?php
namespace  AppBundle\Domain\Note\Entity;

interface Note
{
    /**
     * @return int|string
     */
    public function getId();

    /**
     * @return \DateTime
     */
    public function getCreationDate();

    /**
     * @param \DateTime $creationDate
     */
    public function setCreationDate($creationDate);

    /**
     * @return string
     */
    public function getText();

    /**
     * @param string $text
     */
    public function setText($text);
}
<?php
namespace  AppBundle\Domain\Note\Repository;
use AppBundle\Domain\Note\Entity\Note;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;

class NoteRepositoryDbImplementation implements NoteRepository
{
    const REPOSITORY_NAME = 'AppBundle:notes';

    /** @var EntityManager */
    private $em;
    /** @var EntityRepository */
    private $repository;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->repository = $em->getRepository('AppBundle:notes');
    }

    public function getAll()
    {
        return $this->repository->findAll();
    }

    public function persist(Note $note)
    {
        $this->em->persist($note);
        $this->em->flush();

        return $note;
    }
}
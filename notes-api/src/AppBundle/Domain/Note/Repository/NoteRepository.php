<?php
namespace  AppBundle\Domain\Note\Repository;
use AppBundle\Domain\Note\Entity\Note;

interface NoteRepository
{
    /**
     * @return Note[]
     */
    public function getAll();

    /**
     * @param Note $note
     * @return Note
     */
    public function persist(Note $note);
}
<?php

namespace AppBundle\Controller;

use AppBundle\Domain\Note\Entity\Note;
use AppBundle\Domain\Note\Repository\NoteRepository;
use AppBundle\Entity\notes;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AppBundle\Form\NoteType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/notes")
 */
class ApiController extends FOSRestController
{
    /**
     * @Route("/")
     * @Method( "GET" )
     * @Rest\View
     */
    public function readAction()
    {
        return $this->get('note.note_repository')->getAll();
    }

    /**
     * @Route("/add")
     * @Method( "POST" )
     * @Rest\View
     * @param Request $request
     * @return Note
     */
    public function createAction(Request $request)
    {
        $note = new notes();
        $form = $this->createForm(NoteType::class, $note);
        $form->handleRequest($request);
        if (!$form->isValid()) {
            return View::create(
                array('errorMessage' => 'Validation Failed.', 'errorDetails' => $form->getErrors()->getForm()),
                Response::HTTP_BAD_REQUEST
            );
        }

        if ($note->getCreationDate() === null) {
            $now = $this->get('note.date_time_provider')->get();
            $note->setCreationDate($now);
        }

        /** @var NoteRepository $repository */
        $repository = $this->get('note.note_repository');
        return $repository->persist($note);
    }
}

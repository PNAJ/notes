<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class ApiControllerTest extends WebTestCase
{
    /** @var Client */
    private $client;

    protected function setUp()
    {
        $this->client = static::createClient();
    }

    /**
     * @test
     * @dataProvider headersDataProvide
     * @param array $headers
     */
    public function readResponseInJSON(array $headers)
    {
        $expected = [
            ['id' => 1, 'creation_date' => '2016-01-02T13:14:15+0100', 'text' => 'note 1'],
            ['id' => 2, 'creation_date' => '2016-01-01T13:14:15+0100', 'text' => 'note 2'],
            ['id' => 3, 'creation_date' => '2016-01-01T12:13:14+0100', 'text' => 'note 3'],
        ];
        $this->client->request('GET', '/notes/', [], [], $headers);
        $this->assertEquals(
            Response::HTTP_OK,
            $this->client->getResponse()->getStatusCode()
        );

        $data = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertSame($expected, $data);
    }

    /**
     * @test
     */
    public function readResponseInXML()
    {
        $expected = '<?xmlversion="1.0"encoding="UTF-8"?><result>' .
            '<entry><id>1</id><creation_date><![CDATA[2016-01-02T13:14:15+0100]]></creation_date><text><![CDATA[note1]]></text></entry>' .
            '<entry><id>2</id><creation_date><![CDATA[2016-01-01T13:14:15+0100]]></creation_date><text><![CDATA[note2]]></text></entry>' .
            '<entry><id>3</id><creation_date><![CDATA[2016-01-01T12:13:14+0100]]></creation_date><text><![CDATA[note3]]></text></entry></result>';
        $this->client->request('GET', '/notes/', [], [], ['HTTP_ACCEPT' => 'text/xml']);
        $this->assertEquals(
            Response::HTTP_OK,
            $this->client->getResponse()->getStatusCode()
        );

        $this->assertSame($expected, $string = preg_replace('/\s+/', '', $this->client->getResponse()->getContent()));
    }

    /**
     * @test
     */
    public function readResponseInJSONWhenWrongAcceptHeader()
    {
        $this->markTestSkipped('JSON format for invalid accept header doesn\'t work yet');
        $expected = ['pnaj', 123, [234,345]];
        $this->client->request('GET', '/notes/', [], [], ['HTTP_ACCEPT' => 'INVALID_HEADER']);
        $this->assertEquals(
            Response::HTTP_OK,
            $this->client->getResponse()->getStatusCode()
        );

        $this->assertSame($expected, $this->client->getResponse()->getContent());
    }

    /**
     * @test
     */
    public function createNoteFail()
    {
        $expected = '{"errorMessage":"Validation Failed.","errorDetails":{"children":{"text":{},"creationDate":{}}}}';
        $this->client->request('POST', '/notes/add', [], [], ['HTTP_CONTENT_TYPE' => 'application/json', 'HTTP_ACCEPT' => 'application/json']);
        $this->assertEquals(
            Response::HTTP_BAD_REQUEST,
            $this->client->getResponse()->getStatusCode()
        );
        $this->assertEquals($expected, $this->client->getResponse()->getContent());
    }

    /**
     * @test
     */
    public function createNoteSuccessWithoutDate()
    {
        $date = new \DateTime();
        $post = '{"note":{"text":"new note 123"}}';
        $expected = ['id' => 4, 'creation_date' => $date->format(\DateTime::ISO8601), 'text' => 'new note 123'];

        $dateTime = $this->getMockBuilder('AppBundle\\Domain\\DataTimeProvider\\DateTimeProvider')
            ->disableOriginalConstructor()
            ->setMethods(array('get'))
            ->getMock();
        $dateTime->expects($this->once())
            ->method('get')
            ->will($this->returnValue($date));

        $kernel = $this->createKernel();
        $kernel->boot();
        $kernel->getContainer()->set('note.date_time_provider', $dateTime);

        $client = $kernel->getContainer()->get('test.client');
        $client->setServerParameters(array(), array());
        $client->request('POST', '/notes/add', [], [], ['CONTENT_TYPE' => 'application/json', 'HTTP_ACCEPT' => 'application/json'], $post);
        $this->assertEquals(
            Response::HTTP_OK,
            $client->getResponse()->getStatusCode()
        );

        $data = json_decode($client->getResponse()->getContent(), true);
        $this->assertSame($expected, $data);
    }

    /**
     * @test
     */
    public function createNoteSuccessWithDate()
    {
        $post = '{"note":{"text":"new note 345", "creationDate":"2016-01-01 12:13:14"}}';
        $expected = ['id' => 5, 'creation_date' => '2016-01-01T12:13:14+0100', 'text' => 'new note 345'];

        $this->client->request('POST', '/notes/add', [], [], ['CONTENT_TYPE' => 'application/json', 'HTTP_ACCEPT' => 'application/json'], $post);
        $this->assertEquals(
            Response::HTTP_OK,
            $this->client->getResponse()->getStatusCode()
        );

        $data = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertSame($expected, $data);
    }

    public function headersDataProvide()
    {
        return [
            [['HTTP_ACCEPT' => '']],
            [['HTTP_ACCEPT' => '*/*']],
            [['HTTP_ACCEPT' => 'application/json']],
        ];
    }
}

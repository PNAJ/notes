BEGIN;

delete from notes;
insert into notes(id, creation_date, text) values (1, '2016-01-02 13:14:15', 'note 1');
insert into notes(id, creation_date, text) values (2, '2016-01-01 13:14:15', 'note 2');
insert into notes(id, creation_date, text) values (3, '2016-01-01 12:13:14', 'note 3');

COMMIT;